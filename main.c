#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  short int h, m, s;
} t_time;

typedef struct
{
  t_time *key;
  char *value;
} t_timetable_item;

typedef struct
{
  t_timetable_item *table;
  int size, n;
} t_timetable;

t_time *init(short int h, short int m, short int s);
t_timetable *init_table(int size);
int time_cmp(t_time *ta, t_time *tb);
void put(t_timetable *tt, t_time *key, char *value);
char *get(t_timetable *tt, t_time *key);
void print_table(t_timetable *tt);
int get_h(t_time *t);
int get_m(t_time *t);
int get_s(t_time *t);

int main()
{
  t_time *ta;
  t_timetable *tt;
  size_t len;
  char *str;
  int h, m, s, size;

  printf("Digite o total de itens a serem cadastrados => ");
  scanf("%d", &size);
  tt = init_table(size);

  printf("Informe um horario => ");
  scanf("%d:%d:%d", &h, &m, &s);
  while (h >= 0)
  {
    getchar();

    ta = init(h, m, s);

    printf("Informe a chave para esse horario => ");
    str = NULL;
    len = getline(&str, &len, stdin);
    str[len - 1] = '\0';

    put(tt, ta, str);

    printf("Informe um horario => ");
    scanf("%d:%d:%d", &h, &m, &s);
  }

  print_table(tt);

  scanf("%d:%d:%d", &h, &m, &s);
  while (h >= 0)
  {
    ta = init(h, m, s);
    str = get(tt, ta);

    if (str)
      printf("%02d:%02d:%02d => %s\n", h, m, s, str);
    else
      printf("%02d:%02d:%02d => nao encontrado\n", h, m, s);

    scanf("%d:%d:%d", &h, &m, &s);
  }
}

t_time *init(short int h, short int m, short int s)
{
  t_time *nt;

  if (h < 0 || h > 23)
    return (NULL);
  if (m < 0 || m > 59)
    return (NULL);
  if (s < 0 || s > 59)
    return (NULL);

  nt = (t_time *)malloc(sizeof(t_time));

  nt->h = h;
  nt->m = m;
  nt->s = s;

  return (nt);
}

t_timetable *init_table(int size)
{
  t_timetable *tt;

  tt = (t_timetable *)malloc(sizeof(t_timetable));
  tt->table = (t_timetable_item *)malloc(sizeof(t_timetable_item) * size);
  tt->size = size;
  tt->n = 0;

  return (tt);
}

int time_cmp(t_time *ta, t_time *tb)
{
  int total_sa, total_sb;

  total_sa = ta->h * 3600 + ta->m * 60 + ta->s;
  total_sb = tb->h * 3600 + tb->m * 60 + tb->s;

  if (total_sa > total_sb)
    return (1);
  else if (total_sa < total_sb)
    return (-1);
  else
    return (0);
}

void put(t_timetable *tt, t_time *key, char *value)
{
  int i, j;

  if (tt->n == tt->size)
    exit(-1);

  i = 0;
  while (i < tt->n && time_cmp(key, tt->table[i].key) > 0)
  {
    i++;
  }

  if (tt->n > i && time_cmp(key, tt->table[i].key) != 0)
  {
    for (j = tt->n - 1; j >= i; --j)
    {
      tt->table[j + 1].key = tt->table[j].key;
      tt->table[j + 1].value = tt->table[j].value;
    }
  }

  tt->table[i].key = key;
  tt->table[i].value = value;
  if (tt->n == i)
    tt->n++;
}

char *get(t_timetable *tt, t_time *key)
{
  int lo, hi, m, x;

  lo = 0;
  hi = tt->n - 1;

  while (lo <= hi)
  {
    m = (lo + hi) / 2;

    x = time_cmp(tt->table[m].key, key);
    if (x == 0)
    {
      return (tt->table[m].value);
    }
    else if (x == 1)
    {
      hi = m - 1;
    }
    else
    {
      lo = m + 1;
    }
  }

  return (NULL);
}

void print_table(t_timetable *tt)
{
  int i, h, m, s;
  t_time *t;

  for (i = 0; i < tt->n; ++i)
  {
    t = tt->table[i].key;
    h = get_h(t);
    m = get_m(t);
    s = get_s(t);

    printf("[%02d] - %02d:%02d:%02d => %s\n", i, h, m, s, tt->table[i].value);
  }
}

int get_h(t_time *t) { return t->h; }
int get_m(t_time *t) { return t->m; }
int get_s(t_time *t) { return t->s; }
